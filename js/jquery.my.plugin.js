(function ($) {

    /*------------------------------
        ページスクロール
    --------------------------------*/
    $.pageTopAct = function() {
        var topbutton = $('#page-top');
        /*topbutton.hide();
        $(window).scroll(function() {
            if($(this).scrollTop() > 500) {
                topbutton.fadeIn();
            } else {
                topbutton.fadeOut();
            }
        });*/
        topbutton.click(function() {
            $('body, html').animate({
                scrollTop: 0
            }, 500);
            return false;
        });
    };
    
    /*-------------------------------
        フォントサイズ制御
    ---------------------------------*/
    /*$.fontSizeAct = function() {
        var basesize = $.cookie('saveFontSize');
        if(basesize == null) basesize = "16px";
        $('body').css({'cssText' : 'font-size:' + basesize + '!important'});
        $('li', '#font-resizer').click(function() {
            var cmd = this.id;
            if(cmd == "smaller") {
                var tmp = parseFloat(basesize.replace("px",""));
                var setsize = tmp - 1;
                basesize = String(setsize) + "px";
                $('body').css({'cssText' : 'font-size:' + basesize + '!important'});
                $.cookie('saveFontSize', basesize, { path: '/', expires: 3});
            }
            else if(cmd == "larger") {
                var tmp = parseFloat(basesize.replace("px",""));
                var setsize = tmp + 2;
                basesize = String(setsize) + "px";
                $('body').css({'cssText' : 'font-size:' + basesize + '!important'});
                $.cookie('saveFontSize', basesize, { path: '/', expires: 3});
            }
            else if(cmd == "default") {
                $('body').css({'cssText' : 'font-size: 16px!important'});
                $.cookie('saveFontSize', null,  { path: '/', expires: -1});
            }
        });
        
    };*/

})(jQuery);